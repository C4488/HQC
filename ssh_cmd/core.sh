fnCmdExe(){
    local i p u h j
    for i in `egrep -av $'^[ \t]{0,}#|^[ \t]{0,}$' ${host_list}`
    do  
        p=`echo "${i}" | awk -F',' '{print $2}'`
        u=`echo "${i}" | awk -F',' '{print $1}'`
        h=`echo "${i}" | awk -F',' '{print $3}'`
        ((j++))
        
        if [[ -f "${sshCmd_list}" ]]; then
            PInfo "第 ${j} 台主机 \"$h\": ssh -p $p $u@$h < \033[1;36m""'$1'""\033[0m"
            ssh -p "$p" "$u"@"$h" < "$1" 2>&1 && POk "[命令执行成功] 远程主机名: $h\n\n========\n" || PWarn "[命令未能执行成功] 远程主机名: $h\n\n========\n"
        else
            PInfo "第 ${j} 台主机 \"$h\": ssh -p $p $u@$h \033[1;36m""'$1'""\033[0m"
            ssh -p "$p" "$u"@"$h" "$1" 2>&1 && POk "[命令执行成功] 远程主机名: $h\n\n========\n" || PWarn "[命令未能执行成功] 远程主机名: $h\n\n========\n"
        fi
    done
}