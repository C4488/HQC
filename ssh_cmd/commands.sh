# 开头为'#'的是注释行
# 可以写入多行命令. 可以使用存在于$PATH中的命令,检查方法:执行type command就知道哪些命令可以使用
# 由于sshd调用的是bash -c 'command',而不是初始化出一个Bash,所以不能使用ll这样的alias命令

    # sed命令的常用伎俩1:      # 要注意反斜杠'\'的语法,注意sed 's/a/\t/'和sed 's/a/\\t/'的区别
#s1='warning="Bad: ${hostName} `cat ${checkJvmGCTemp}`"'
#s2='      jstack -l ${tomcatPID} > ${healthCheckTemp}/jstack.temp || jstack -F -m -l ${tomcatPID} > ${healthCheckTemp}/jstack.temp'
#s3='/usr/src/shellScripts/healthCheck/healthCheckD.sh'
#sed -i "/${s1}/i ${s2}" ${s3}; grep -A5 -B5 'jstack' ${s3}

    # sed命令的常用伎俩2:  对于替换命令's','/'前要加'\'.总之要注意sed的语法,其它命令亦然.或者用s%src%des%g代替s/src/des/g
#s1='"${body}" "${healthCheckTemp}\/jstack.temp"'
#s2='"${body}"'
#s3='/usr/src/shellScripts/healthCheck/healthCheckD.sh'
#sed -i "181s/${s2}/${s1}/" ${s3}; grep -A5 -B5 'jstack.temp' ${s3}

#s1='touch /var/lock/subsys/local'                      # 用下面的方法修改/etc/rc.local是不正确的,因为sed命令并不follow软链接
#s2='# ps -ef |egrep "healthCheckD|apache-tomcat"'      # 因不支持alias,因此加上--color支持彩色显示
#sed -i "s%${s1}%${s2}%" /etc/rc.local; grep --color=always touch /etc/rc.local         # 应把/etc/rc.local改为/etc/rc.d/rc.local

    # 常用命令,使用时取消开头的'#'
ps -ef |grep --color=always grep