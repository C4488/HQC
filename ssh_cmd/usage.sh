Usage()
{
    echo -e "
    /*
     * 说明：
     * 1.${pro_name} 是指 在已经部署了 SSH 免密登陆的远程主机上执行 commands
     * 2.通常不需修改 [配置文件]:     ${pro_base}/${pro_name}/${pro_name}.conf
     * 3.通常需要修改 [远程主机清单]: ${pro_base}/${pro_name}/${pro_name}.host
     * 4.通常需要修改 [命令列表]:     ${pro_base}/${pro_name}/commands.sh
     * Usage: 
     * 方法1. 执行: \033[1mbash $0 '${pro_base}/${pro_name}/commands.sh'\033[0m
     * 方法2. 执行: \033[1mbash $0 'cmd1 && cmd2 && ...'\033[0m
     */
    "
}                   # $0 总是shell脚本的'路径名/文件名'

Help()              # 若存在 -h 或 --help 选项，则输出 Usage
{
    local i;
    for i
    do
        case $i in
            -h|--help)
                Usage; exit 0;;
            *)
                : ;;
        esac
    done
}