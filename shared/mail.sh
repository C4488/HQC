# 用于发邮件给自己或其他人,变量以小写字母开头,函数以大写字母开头
# 注意:邮件正文mail_body是有长度限制的(肯定要小于 135506 Byte),过长的话会报错:Argument list too long.太长太大的日志文件,请用附件的形式发送
# 有时收到邮件的日期是错的,所以在subject加上日期以确定准确的发件日期

fromMailAddr="${fromMailAddr:=sender@okps.cn}"  # 发件人地址
smtpServer="${smtpServer:=smtp.exmail.qq.com}"  # smtp地址
mail_psw="${mail_psw:=sender_password}"         # smtp密码
# example: node mail_from_qq.js "13925464488@139.com,yjt@okps.cn" "subject" "mail_body" "/path/attachment1,/path/attachment2"
# "mail_body" could be "`Change_to_html ${some file}`"
MailToAny(){
    if node /usr/src/HQC/shared/nodemailer/mail_from_qq.js "$fromMailAddr" "$1" "$smtpServer" "$mail_psw" "$2 at $(date +'%FT%T')" "
<html>
<head><meta charset=\"utf-8\">
<style type=\"text/css\">
span.PInfo{ color:SandyBrown; }
span.PError{ color:Red; }
span.POk{ color:YellowGreen; }
span.PWarn{ color:Fuchsia; }
span.PNote{ color:DodgerBlue; }
span.EvalCont{ color:SeaGreen; }
span.HighLight{ color:Black; }
</style></head>
<body><pre>$3</pre></body>
</html>" "$4"; then
        echo "邮件发送成功,主题是:$2"
    else
        echo "邮件发送失败,主题是:$2"
        return 1
    fi
}

Change_to_html(){
    awk '
    {
        gsub(/</,"\\&lt;");
        t=gensub(/&lt;(br|hr|TD|TR|TH|TABLE|\/|b>)/,"<\\1","g");  # HTML代码中,"<"用"&lt;"表示;此处当然不完整,以后可能还要加上对<a></a>的转换
        gsub(/\033\[1;33m/, "<span class=\"PInfo\"><b>",t);       # 对应 PInfo
        gsub(/\033\[1;31m/, "<span class=\"PError\"><b>",t);      # 对应 PError
        gsub(/\033\[1;32m/, "<span class=\"POk\"><b>",t);         # 对应 POk
        gsub(/\033\[1;35m/, "<span class=\"PWarn\"><b>",t);       # 对应 PWarn
        gsub(/\033\[1;36m/, "<span class=\"PNote\"><b>",t);       # 对应 PNote
        gsub(/\033\[36m/,   "<span class=\"EvalCont\"><b>",t);    # 对应 Eval_cont 中的 \033[36m
        gsub(/\033\[1m/,    "<span class=\"HighLight\"><b>",t);   # 对应 高亮的 \033[1m
        gsub(/\033\[0m/,    "</b></span>",t);                     # 对应 \033[0m
        print t
    }' "$@"
}
