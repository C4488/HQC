# 黄色:向用户提示普通信息,例如:"将要执行...指令"等
PInfo(){
  echo -e "[ \033[1;33mINFO\033[0m ] `date '+%FT%T'` $@"
}

# 红色:失败 或 告警 类 提示
PError(){
  echo -e "[ \033[1;31mERROR\033[0m ] `date '+%FT%T'` $@"
}

# 绿色:指令执行成功 类 提示
POk(){
  echo -e "[ \033[1;32m OK \033[0m ] `date '+%FT%T'` $@"
}

# 紫色 warning info
PWarn(){
  echo -e "[ \033[1;35mWARN\033[0m ] `date '+%FT%T'` $@"
}

# 蓝色:read -p "`PNote` PID: "等交互 类 提示
PNote(){
  echo -e "[ \033[1;36mNOTE\033[0m ] `date '+%FT%T'` $@"
}

Eval_exit(){
    PInfo "Running: \033[36m$@\033[0m"
    ! eval "$@" && PError "Fail to run: \033[36m$@\033[0m" && exit 1
    return 0
}

Eval_cont(){
    PInfo "Running: \033[36m$@\033[0m"
    eval "$@"
}
export -f PInfo PError POk PWarn PNote Eval_exit Eval_cont
