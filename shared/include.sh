Include(){
    local i;
    for i in "$@"
    do
        ! source "$i" && echo -e "[ \033[1;31mERROR\033[0m ] `date '+%FT%T'` Failed to 'source $i'." && exit 1 ||
            echo -e "[ \033[1;32m OK \033[0m ] `date '+%FT%T'` source $i"
    done
}

