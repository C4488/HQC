#!/usr/bin/bash
pro_base="$(cd `dirname $(readlink -f $0)`;pwd)"        # program base directory. readlink是为了获取脚本的真实路径
pro_root="${pro_base##*/}"                              # 我的程序的根目录名字.   readlink使得ln创建的软链也能正确地执行
pro_name="$(readlink -f $0 | sed 's#^.*/##; s#.sh$##')" # 删除'/'前的路径,删除'.sh'

source ${pro_base}/shared/include.sh || exit 1
Include ${pro_base}/${pro_name}/usage.sh
Include ${pro_base}/shared/prompt.sh

Help "$@"
[[ $# -lt 1 ]] && Usage && exit 1

Check_tomcat(){
    local tomcat_log="$1"
    if [[ ! -f ${tomcat_log} ]]; then
        PError "缺失文件: [tomcat日志] ${tomcat_log}"
        exit 1
    fi
    awk -f ${pro_base}/tomcat/catalina.awk ${tomcat_log} |less -iR
}

case $1 in
    tomcat|tomca|tomc|tom|to|t)
        [[ $2 == "" ]] && Usage && exit 1
        Eval_cont "Check_tomcat '$2'"
    ;;
    *)
        Usage && exit 1
    ;;
esac
