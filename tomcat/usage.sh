Usage()
{
    echo -e "
    /*
     * 说明：
     * 1.请按需修改 [配置文件]: ${pro_base}/${pro_name}/${pro_name}.conf
     * Usage: 
     * 执行: \033[1mbash $0 {stop|start|restart|-h|--help}\033[0m
     * 执行: \033[1mbash $0 { -k | -s | -r | -h | --help }\033[0m
     */
    "
}                   # $0 总是shell脚本的'路径名/文件名'

Help()              # 若存在 -h 或 --help 选项，则输出 Usage
{
    local i;
    for i
    do
        case $i in
            -h|--help)
                Usage; exit 0;;
            *)
                : ;;
        esac
    done
}