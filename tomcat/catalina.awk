BEGIN{IGNORECASE=1} {
    if(/Server startup in/ || /Deploying web/ || /INFO: Initializing/)
        {print "\033[1;33m" $0 "\033[0m"}
    else if(/error/)
        {print "\033[1;31m" $0 "\033[0m"}
    else if(/\<exception/ || /\<warn/)
        {gsub(/\<exception|\<warn/,"\033[1;35m&\033[0m"); print}
    else {print $0}
}
# \<exception 的 \< 是:单词边界匹配