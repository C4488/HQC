#!/usr/bin/bash
pro_base="$(cd `dirname $(readlink -f $0)`;pwd)"        # program base directory. readlink是为了获取脚本的真实路径
pro_root="${pro_base##*/}"                              # 我的程序的根目录名字.   readlink使得ln创建的软链也能正确地执行
pro_name="$(readlink -f $0 | sed 's#^.*/##; s#.sh$##')" # 删除'/'前的路径,删除'.sh'

source ${pro_base}/shared/include.sh || exit 1
Include ${pro_base}/${pro_name}/${pro_name}.conf ${pro_base}/${pro_name}/{usage,core}.sh
Include ${pro_base}/shared/prompt.sh

Help "$@"
[[ $# -ne 1 ]] && Usage && exit 1

[[ -d "${log_dir}" ]] || Eval_exit "mkdir -p '${log_dir}'"
host_list="${pro_base}/${pro_name}/${pro_name}.host"
sshCmd_list="$1"
{
! [[ -f "${host_list}" ]] && PError "文件缺失: [主机清单] ${host_list}" && exit 1
if [[ -f "${sshCmd_list}" ]]; then
    for i in `egrep -av $'^[ \t]{0,}#|^[ \t]{0,}$' ${host_list}`  # 将注释行和空白行去掉
    do
        if [[ `echo $i |awk -F, '{print NF}'` != 3 ]]; then
            PError "格式不规范: ['主机清单'内容有误] 请修改'${host_list}',下面的内容不规范\n\t${i}"
            exit 1
        fi
    done
    [[ `grep -aEcv $'^[ \t]{0,}#|^[ \t]{0,}$' ${sshCmd_list}` == 0 ]] && PWarn "不在远程主机上执行任何命令: [空文件] ${sshCmd_list}" && exit 1
    fnCmdExe "${sshCmd_list}"
else
    fnCmdExe "${sshCmd_list}"
fi
} |& tee -a "${stdout_log}"