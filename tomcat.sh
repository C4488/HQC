#!/usr/bin/bash
pro_base="$(cd `dirname $(readlink -f $0)`;pwd)"        # program base directory. readlink是为了获取脚本的真实路径
pro_root="${pro_base##*/}"                              # 我的程序的根目录名字.   readlink使得ln创建的软链也能正确地执行
pro_name="$(readlink -f $0 | sed 's#^.*/##; s#.sh$##')" # 删除'/'前的路径,删除'.sh'

source ${pro_base}/shared/include.sh || exit 1
Include ${pro_base}/${pro_name}/{usage.sh,${pro_name}.conf}
Include ${pro_base}/shared/prompt.sh

export pro_base pro_root pro_name tomcatPath tomcatLogPath catalinaLog
export -f Usage Help PInfo PError POk PWarn PNote Eval_exit Eval_cont

bash -c "bash ${pro_base}/tomcat/tomcat_main.sh $@ &"
#Eval_cont "less -iRSM +F ${catalinaLog}"
awk_script="${pro_base}/${pro_name}/catalina.awk"                   # [[ "$ppid" =~ "rc.local" ]]时,说明是由 /etc/rc.local 调用的
ppid="$(ps h -o cmd -p $PPID)"                                      # [[ "$ppid" =~ "sh -c" ]]时,说明是由 crond 调用的
[[ "$ppid" =~ "rc.local" || "$ppid" =~ "sh -c" ]] || tail -f "${catalinaLog}" |awk -f "${awk_script}"
# tail -f不能用less +F代替.可能因为less是分页程序,而分页程序后面接管道是没有意义的
