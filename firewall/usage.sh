Usage()
{
    echo -e "
    /*
     * 说明：
     * 1.本 bash 脚本用于{设置|显示}Linux操作系统的 netfilter 防火墙规则
     * 2.请按需修改 [配置文件]: ${pro_base}/${pro_name}/core.sh
     * Usage: 
     *   \033[1mbash $0 {set|s|display|d|clean|c|-h|--help}\033[0m
     * 
     * 提示：iptables-save > rules.filename 命令可保存防火墙规则到文件
     *    iptables-restore < rules.filename 命令可从文件恢复规则
     */
    "
}                   # $0 总是shell脚本的'路径名/文件名'

Help()              # 若存在 -h 或 --help 选项，则输出 Usage
{
    local i;
    for i
    do
        case $i in
            -h|--help)
                Usage; exit 0;;
            *)
                : ;;
        esac
    done
}
