#!/usr/bin/bash
pro_base="$(cd `dirname $(readlink -f $0)`;pwd)"        # program base directory. readlink是为了获取脚本的真实路径
#pro_root="${pro_base##*/}"                              # 我的程序的根目录名字.   readlink使得ln创建的软链也能正确地执行
#pro_name="$(readlink -f $0 | sed 's#^.*/##; s#.sh$##')" # 删除'/'前的路径,删除'.sh'

source ${pro_base}/shared/include.sh || exit 1
Include ${pro_base}/shared/prompt.sh

zabbix_listen_ip=139.180.136.101        # 若 zabbix server 与 zabbix client 在同一局域网内,就写私网IP,否则就写公网IP
zabbix_server_ip=219.135.128.170
client_listen_port=7777                 # 默认的监听端口是10050

rpm --quiet -q zabbix-release || Eval_exit "rpm -ivh http://repo.zabbix.com/zabbix/3.2/rhel/7/x86_64/zabbix-release-3.2-1.el7.noarch.rpm"
Eval_exit "yum -y install zabbix-agent"
Eval_exit "sed -i 's/Hostname=Zabbix server/Hostname=${zabbix_listen_ip}/' /etc/zabbix/zabbix_agentd.conf && grep -i 'hostname=' /etc/zabbix/zabbix_agentd.conf"
Eval_exit "sed -i '/ListenPort=10050/a ListenPort=${client_listen_port}' /etc/zabbix/zabbix_agentd.conf && grep -i listenport /etc/zabbix/zabbix_agentd.conf"
Eval_exit "sed -i 's/Server=127.0.0.1/Server=${zabbix_server_ip}/' /etc/zabbix/zabbix_agentd.conf && grep 'Server=' /etc/zabbix/zabbix_agentd.conf"
Eval_exit "sed -i 's/ServerActive=127.0.0.1/ServerActive=${zabbix_server_ip}/' /etc/zabbix/zabbix_agentd.conf && grep 'ServerActive=' /etc/zabbix/zabbix_agentd.conf"
    # 修改配置文件,使自定义脚本可以包含特殊字符
Eval_exit "sed -i '/UnsafeUserParameters=/a UnsafeUserParameters=1' /etc/zabbix/zabbix_agentd.conf && grep 'UnsafeUserParameters=' /etc/zabbix/zabbix_agentd.conf"
Eval_exit "systemctl enable zabbix-agent.service; systemctl restart zabbix-agent.service; systemctl status zabbix-agent.service"
