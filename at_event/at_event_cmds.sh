#pro_base=                    # 从父shell继承
#pro_root=                    # 从父shell继承
#pro_name=                    # 从父shell继承
#job_name=                    # 从父shell继承
temp_log="/tmp/${job_name}.$(date +%Y%m%d).log"
at_log_dir="/var/log/${pro_root}/${pro_name}"
at_log="${at_log_dir}/${job_name}.$(date +%Y%m%d).log"

{
source ${pro_base}/shared/include.sh || exit 1
Include ${pro_base}/shared/{prompt.sh,mail.sh}
Include ${pro_base}/${pro_name}/${pro_name}.conf
if ! [[ -d ${at_log_dir} ]]; then
    Eval_exit "mkdir -p ${at_log_dir}"
fi
} &>> ${temp_log}
rm -f ${temp_log}

{
# # #
PInfo "开始: [at 任务] ${job_name}"
# 中间可以是任何需要处理的事情(或需要执行的命令),如
# Eval_exit "sleep 2; echo test 'Eval_exit'"
# Eval_cont "echo test 'Eval_cont'"
POk "完成: [at 任务] ${job_name}"
# # #
if [[ ${mail_or_not} != 0 ]]; then
    MailToAny "${send_to}" "[ OK ] 完成 at 任务: ${job_name}" "`Change_to_html ${at_log}`"
fi
} &>> ${at_log}
# 或者重定向到/dev/null
# 若不将output重定向,则atd将会使用系统默认的邮件系统发邮件通知管理员
