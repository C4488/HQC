Usage()
{
    echo -e "
    /*
     * 说明：at_event 是指用 /usr/bin/at 命令调用的定时事件
     * at 与 crond 的其中一个区别是：at 并不循环执行，只在指定的时间执行一次
     * Usage: 
     * 1.按需修改\${cmd_file}[执行脚本],可参考: ${pro_base}/${pro_name}/at_event_cmds.sh
     * 2.按需修改[配置文件]:                   ${pro_base}/${pro_name}/${pro_name}.conf
     * 3.执行: \033[1mbash $0 \${cmd_file} '\${start_time}' '\${job_name}'\033[0m
     *   执行: \033[1mbash $0 -i\033[0m         # -i: 安装依赖包
     *   提示: 1.第三个参数\${job_name}可以是中文，但不要有空格
     *         2.第二个参数的格式可以参考 grep start_time $0
     */
    "
}                   # $0 总是shell脚本的'路径名/文件名'

Help()              # 若存在 -h 或 --help 选项，则输出 Usage
{
    local i;
    for i
    do
        case $i in
            -h|--help)
                Usage; exit 0;;
            *)
                : ;;
        esac
    done
}
