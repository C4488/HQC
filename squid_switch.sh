#!/usr/bin/bash
pro_base="$(cd `dirname $(readlink -f $0)`;pwd)"
    # program base directory. readlink是为了获取脚本的真实路径
pro_root="${pro_base##*/}"
    # 程序的根目录(脚本的父目录). readlink使得ln创建的软链也能正确地执行
pro_name="$(readlink -f $0 | sed 's#^.*/##; s#.sh$##')"
    # sed命令: 1.删除'/'前的路径 2.删除'.sh'
PInfo(){
  echo -e "[ \033[1;33mINFO\033[0m ] `date '+%FT%T'` $@"
}
Eval_cont(){
    PInfo "Running: \033[36m$@\033[0m"
    eval "$@"
}

Usage()
{
    echo -e "
    /*
     * 说明: 该脚本的作用是使squid在匿名模式和认证模式之间切换
     * Usage: 
     * 执行: \033[1mbash $0 {0|1|'ip/24'|-h|--help}\033[0m
     *                      0: 匿名模式
     *                      1: 认证模式
     *                      'ip/24': 允许该ip的代理请求,例如'223.5.5.5/24'
     */
    "
}                   # $0 总是shell脚本的'路径名/文件名'

Help()              # 若存在 -h 或 --help 选项，则输出 Usage
{
    local i;
    for i
    do
        case $i in
            -h|--help)
                Usage; exit 0;;
            *)
                : ;;
        esac
    done
}
# 参数少于1个,报使用方法
[[ $# -ne 1 ]] && Usage && exit 1
Help "$@"

# $1为0,则是匿名模式,不为0,则是认证模式
case $1 in
    0)
        sed -i 's/^http_access allow Safe_IPs CE.*/http_access allow Safe_IPs/' /etc/squid/squid.conf
        Eval_cont systemctl restart squid
        Eval_cont "grep --color=auto 'Safe_IPs' /etc/squid/squid.conf"
    ;;
    1)
        sed -i 's/^http_access allow Safe_IPs.*/http_access allow Safe_IPs CE/' /etc/squid/squid.conf
        Eval_cont systemctl restart squid
        Eval_cont "grep --color=auto 'Safe_IPs' /etc/squid/squid.conf"
    ;;
    *)
        sed -i "/acl SSL_ports port 443/i acl Safe_IPs src $1" /etc/squid/squid.conf
        Eval_cont systemctl restart squid
        Eval_cont "grep --color=auto 'Safe_IPs' /etc/squid/squid.conf"
    ;;
esac