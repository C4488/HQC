#!/usr/bin/bash
pro_base="$(cd `dirname $(readlink -f $0)`;pwd)"        # program base directory. readlink是为了获取脚本的真实路径
#pro_root="${pro_base##*/}"                              # 我的程序的根目录名字.   readlink使得ln创建的软链也能正确地执行
#pro_name="$(readlink -f $0 | sed 's#^.*/##; s#.sh$##')" # 删除'/'前的路径,删除'.sh'

source ${pro_base}/shared/include.sh || exit 1
Include ${pro_base}/shared/prompt.sh

fnEchoUsage(){
    PNote "Usage: bash $0 {shutdown|-s|restart|-r}" && exit 1
}

fnHelp()
{
    local i;
    for i
    do
        case $i in
            -h|--help)
                fnEchoUsage; exit 0;;
            *)
                : ;;
        esac
    done
}

fnHelp "$@"

[[ "$1" == "shutdown" || "$1" == "-s" || "$1" == "restart" || "$1" == "-r" ]] || fnEchoUsage

sync;
PInfo "killing smon processes."
pid="$(ps -ef|grep -E 'smon'|grep -v grep|awk '{print $2}')"
[[ "$pid" != "" ]] && kill $pid
[[ -f /mnt/pbkb/node_pbkb.sh ]] && bash /mnt/pbkb/node_pbkb.sh -k
[[ -f /mnt/pbkb/node_info.sh ]] && bash /mnt/pbkb/node_info.sh -k
[[ -f /mnt/pbkb/node_company.sh ]] && bash /mnt/pbkb/node_company.sh -k
[[ -f /mnt/pbkb/node_bdic.sh ]] && bash /mnt/pbkb/node_bdic.sh -k
[[ -f /mnt/pbkb/node_operation.sh ]] && bash /mnt/pbkb/node_operation.sh -k
Eval_cont "bash ${pro_base}/tomcat.sh -k"
PInfo "tomcat is stopped."
Eval_cont 'systemctl stop httpd nginx rh-nginx116-nginx mysqld mariadb vsftpd'
Eval_cont 'systemctl stop rh-php72-php-fpm php-fpm zabbix-agent zabbix-server redis mysqlrouter vnstat smb nmb'

if [[ $(df -T |egrep -c 'cifs|iso9660') != 0 ]]; then
    PInfo "umounting 'cifs' and 'iso9660' partitions."
    ! df -T |awk '/cifs|iso9660/{print $NF}'|xargs -i umount '{}' && echo "Failed to umount some partitions, now exit" && exit 1
fi

sync; sync
[[ "$1" == "restart" || "$1" == "-r" ]] && Eval_cont "init 6" || Eval_cont "init 0"
