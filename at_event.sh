#!/usr/bin/bash
pro_base="$(cd `dirname $(readlink -f $0)`;pwd)"        # program base directory. readlink是为了获取脚本的真实路径
pro_root="${pro_base##*/}"                              # 我的程序的根目录名字.   readlink使得ln创建的软链也能正确地执行
pro_name="$(readlink -f $0 | sed 's#^.*/##; s#.sh$##')" # 删除'/'前的路径,删除'.sh'
cmd_file="$1"
start_time="$2"
job_name="$3"                           # 起个自己容易记的任务名,写日志时,用 job_name 区分每个任务
#start_time的格式请参考man_at,示例如下:
#start_time='17:00'
#start_time='now + 2 hours' / 'now + 1 minute'
#start_time='10:00 2018-08-10'
#start_time='16:00 + 5 days' / 可以不用复数形式'16:00 + 5 day'
export pro_base pro_root pro_name job_name

source ${pro_base}/shared/include.sh || exit 1
Include ${pro_base}/${pro_name}/usage.sh
Include ${pro_base}/shared/prompt.sh

Install_dependence(){
    Eval_cont "type at" || Eval_exit "yum install -y at"
    Eval_exit "systemctl enable atd.service && systemctl restart atd.service"
    POk "依赖包,依赖条件已安装"
}

Help "$@"
if [[ $1 == "-i" ]]; then
    Install_dependence
    exit
fi

[[ $# -lt 3 ]] && Usage && exit 1

! [[ -f "${cmd_file}" ]] && echo -e "[ \033[1;31mERROR\033[0m ] 退出程序: [缺失脚本文件] ${cmd_file}" && exit 1
at -f "${cmd_file}" "${start_time}"
# 等价于 cat "${cmd_file}" | at "${start_time}",即使把${cmd_file}删除,任务也能执行
# 显示有多少任务,请执行: atq
# 显示任务的详细代码: at -c job_number
# 删除1号任务,请执行: atrm 1
[[ $? != 0 ]] && echo -e "[ \033[1;31mERROR\033[0m ] 退出程序: [ at 命令出错]." && exit 1
