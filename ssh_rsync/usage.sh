Usage()
{
    echo -e "
    /*
     * 说明：
     * 1.${pro_name} 是指 把一个或多个'目录|文件'通过 rsync 传输到已经部署了 SSH 免密登陆的远程主机
     * 2.通常不需修改 [配置文件]:        ${pro_base}/${pro_name}/${pro_name}.conf
     * 3.通常需要修改 [远程主机清单]:    ${pro_base}/${pro_name}/${pro_name}.host
     * 4.通常需要修改 [排除pattern文件]: ${pro_base}/${pro_name}/${pro_name}.exclude
     * Usage: 
     * 1.执行: \033[1mbash $0 '绝对路径1' '绝对路径2' ...\033[0m
     *   执行: \033[1mbash $0 -i\033[0m  # -i: install依赖包
     */
    "
}                   # $0 总是shell脚本的'路径名/文件名'

Help()              # 若存在 -h 或 --help 选项，则输出 Usage
{
    local i;
    for i
    do
        case $i in
            -h|--help)
                Usage; exit 0;;
            *)
                : ;;
        esac
    done
}