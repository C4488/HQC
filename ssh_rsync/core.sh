fnEchoSepLine(){
    echo -e '\n========== 分割线 ==========\n'
}

fnCheckArgs(){
    local i
    for i
    do
        ! [[ -a "$i" ]] && PError "[文件或目录缺失] '$i'" && exit 1
        ! [[ "${i:0:1}" == "/" ]] && PError "[仅支持绝对路径] 不符要求的参数: $i" && exit 1    # 限定绝对路径
    done
}

fnRsyncToRemote(){
    local i j r f=1                         # r: remote host; f: file
    for i
    do
        r=0                                 # 如果同时同步多个目录或文件,初始化k
        for j in $(egrep -av $'^[ \t]{0,}#|^[ \t]{0,}$' ${host_list})
        do
            ((r++))
            h=$(echo $j |awk -F, '{print $2}')
            p=$(echo $j |awk -F, '{print $1}')
            PInfo "第 $f 个文件或目录 --> 第 $r 台主机 \"$h\":"
            # 详细信息输出至另一个log
            if rsync -aRv --del --exclude-from="${exclude_file}" -e "ssh -p ${p}" "$i" ${h}:/ &>> "${verbose_temp}"; then
                POk "成功传输 '$i' 至 '${h}:${i}'"
                fnEchoSepLine
            else
                PError "失败传输 '$i' 至 '${h}:${i}'"
                fnEchoSepLine
            fi
        done
        ((f++))
    done
}

fnInstallDependence(){
    local j
    Eval_cont "type rsync" || Eval_exit "yum install -y rsync"
    for j in $(egrep -av $'^[ \t]{0,}#|^[ \t]{0,}$' ${host_list})
    do
        h=$(echo $j |awk -F, '{print $2}')
        p=$(echo $j |awk -F, '{print $1}')
        Eval_cont "ssh -p ${p} ${h} 'type rsync'" || Eval_exit "ssh -p ${p} ${h} 'yum install -y rsync'"
    done
    POk "依赖包已成功安装"
}