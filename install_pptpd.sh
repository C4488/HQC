#!/usr/bin/bash
pro_base="$(cd `dirname $(readlink -f $0)`;pwd)"        # program base directory. readlink是为了获取脚本的真实路径
#pro_root="${pro_base##*/}"                              # 我的程序的根目录名字.   readlink使得ln创建的软链也能正确地执行
#pro_name="$(readlink -f $0 | sed 's#^.*/##; s#.sh$##')" # 删除'/'前的路径,删除'.sh'

source ${pro_base}/shared/include.sh || exit 1
Include ${pro_base}/shared/prompt.sh

Eval_exit "yum install -y ppp pptpd"

echo -e "localip 192.168.99.1\nremoteip 192.168.99.11-30" >>/etc/pptpd.conf

echo "chq99   pptpd   99chq   192.168.99.99
chq98   pptpd   98chq   192.168.99.98
chq97   pptpd   97chq   192.168.99.97
chq96   pptpd   96chq   192.168.99.96
chq95   pptpd   95chq   192.168.99.95
chq94   pptpd   94chq   192.168.99.94
chq93   pptpd   93chq   192.168.99.93
chq92   pptpd   92chq   192.168.99.92
chq91   pptpd   91chq   192.168.99.91
chq90   pptpd   90chq   192.168.99.90" >>/etc/ppp/chap-secrets

if [[ `sysctl -n net.ipv4.ip_forward` == 0 ]]; then
    sed -i '/net.ipv4.ip_forward/d' /etc/sysctl.conf
    echo 'net.ipv4.ip_forward=1' >> /etc/sysctl.conf
    sysctl -p               # 相当于 sysctl -p'/etc/sysctl.conf'
fi

sed -i '/# 这中间可以执行任何其他代码或脚本/a Eval_cont "systemctl status pptpd"' /etc/rc.d/rc.local

systemctl enable pptpd &
systemctl restart pptpd &
Eval_cont "tail -f /var/log/messages"
