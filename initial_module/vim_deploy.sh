Eval_cont dnf install vim -y
sed -i '/^set ignorecase/d; 
/^set ic/d;
/^set nu/d; 
/^set ts/d; 
/^set is/d; 
/^set sw/d; 
/^set paste/d;
/^set expandtab/d;
/^set cul/d;' /etc/vimrc # cul:下划线标出当前行

# vimrc中的注释符是双引号，不是井号
echo '
set ic        " 查找不区分大小写
set nu        " 显示行号
set ts=4      " tab的宽度为4个空格
set sw=4      " <<,>>操作符的平移量为4个空格
set is        " 动态高亮搜索字符串
set paste     " 粘贴模式,禁用自动注释
set expandtab " 转换tab为空格' >> /etc/vimrc    # 提示:敲代码时可以':set ai'设置自动缩进,但粘贴代码时就要':set noai'
Eval_cont 'tail /etc/vimrc'
