Usage()
{
    echo -e "
    /*
     * 说明：
     * 1. ${pro_name} 是指: system monitor
     * 1.请按需修改 [配置文件]: ${pro_base}/${pro_name}/{general.conf|special.conf}
     * Usage: 
     * 执行: \033[1mbash $0 {-i|-b|-f|-r|-k|-l|-h|--help}\033[0m
     *                      -i: install依赖包
     *                      -b: 后台执行时使用该option(比如在rc.local执行)
     *                      -f: 前台启动进程,用tail -f \${stdout_log}查看日志
     *                      -r: 前台 restart 进程,用tail -f \${stdout_log}查看日志
     *                      -k: kill 进程
     *                      -l: reload '${pro_base}/${pro_name}'中的配置文件 general.conf 和 special.conf
     */
    "
}                   # $0 总是shell脚本的'路径名/文件名'

Help()              # 若存在 -h 或 --help 选项，则输出 Usage
{
    local i;
    for i
    do
        case $i in
            -h|--help)
                Usage; exit 0;;
            *)
                : ;;
        esac
    done
}