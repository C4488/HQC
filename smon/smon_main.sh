trap "Trap_1" 1

GOOD=1
BAD=0
awk_warn="\033[1;35mWARN\033[0m"

Source
Include ${pro_base}/shared/mail.sh
Include ${pro_base}/${pro_name}/general.{sh,conf}
if [[ ${special} != 0 ]]; then
    # 检查目录,不存在则退出
    Include ${pro_base}/${pro_name}/special.{sh,conf}
    [[ ! -d ${zabbix_tmp} ]] && echo "[缺失zabbix临时目录]: ${zabbix_tmp}.退出程序." && exit 1
fi

# 处理SIGHUP信号
Trap_1(){
    unset $(egrep -av $'^[ \t]{0,}#|^[ \t]{0,}$' ${pro_base}/${pro_name}/${pro_name}.conf |awk -F'=' '{print $1}')
    Include ${pro_base}/${pro_name}/general.conf
    [[ ${special} != 0 ]] && Include ${pro_base}/${pro_name}/special.conf
}

while :
do
    ((c++))                                             # c: count 正在执行第几次循环,可以考虑把下面的任务放到后台执行,但要做好并发控制
    PInfo "====================\033[1;36m Execute $c times \033[0m===================="
    [[ ${checkDisk} != "" ]] && Eval_cont C_disk ${checkDisk}
    [[ ${checkCPU} != "" ]] && Eval_cont C_CPU ${checkCPU}
    [[ ${checkSWAP} != "" ]] && Eval_cont C_swap ${checkSWAP}
    [[ ${checkPort} != "" ]] && Eval_cont C_port ${checkPort}
    [[ ${checkNetSpeed} != "" ]] && Eval_cont C_nspeed ${checkNetSpeed}
    [[ ${checkRemoteHost} != "" ]] && Eval_cont C_rhost ${checkRemoteHost}
    [[ "${checkMessageLogKeys}" != "" ]] && Eval_cont "C_message '${messageLog}' '${checkMessageLogKeys}'"

####################################################################
# 上面是常规检查项
# 下面是为特定的程序(如:nginx, tomcat, mysql等)写的检查项
####################################################################

if [[ ${special} != 0 ]]; then
    [[ "${checkCatalinaLogKeys}" != "" ]] && Eval_cont "C_catalina '${catalinaLog}' '${checkCatalinaLogKeys}'"
    [[ "${checkJvmGCVal}" != "" ]] && Eval_cont C_jvmGC ${checkJvmGCVal}
    [[ "${mysql_lock_trx}" != "" ]] && Eval_cont C_mysql_lock ${mysql_lock_trx}
fi
    sleep ${sleepTime}
done
