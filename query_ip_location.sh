Usage(){
    echo "Usage: bash $0 [taobao] ip1 [ip2] ..." && exit 1
}

(($# < 1)) && Usage

if [[ $1 == taobao ]]; then                                     # 用taobao的API查询
    (($# < 2)) && Usage
    for ((i=2;i<=$#;i++))
    do
        curl http://ip.taobao.com/service/getIpInfo.php?ip=${!i}
    done
else                                                            # 用ipip.net的API查询
    for i; do
        query_ip="$(curl http://freeapi.ipip.net/$i 2>/dev/null)"
        echo "'$i' locates in: $query_ip"
        sleep 1                                                 # 由于网站限速,不能连续查询
    done
fi

# taobao返回
if false; then
{"code":0,"data":{"ip":"8.8.8.8","country":"美国","area":"","region":"XX","city":"XX","county":"XX","isp":"Level3","country_id":"US","area_id":"","region_id":"xx","city_id":"xx","county_id":"xx","isp_id":"200053"}}
{"code":0,"data":{"ip":"112.193.121.235","country":"中国","area":"","region":"四川","city":"成都","county":"XX","isp":"联通","country_id":"CN","area_id":"","region_id":"510000","city_id":"510100","county_id":"xx","isp_id":"100026"}}
{"code":0,"data":{"ip":"223.5.5.5","country":"中国","area":"","region":"浙江","city":"杭州","county":"XX","isp":"阿里云","country_id":"CN","area_id":"","region_id":"330000","city_id":"330100","county_id":"xx","isp_id":"1000323"}}
{"code":0,"data":{"ip":"165.225.157.157","country":"美国","area":"","region":"内华达","city":"XX","county":"XX","isp":"XX","country_id":"US","area_id":"","region_id":"US_128","city_id":"xx","county_id":"xx","isp_id":"xx"}}
fi
