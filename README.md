# HQC的运维脚本

#### 介绍
HQC（HaoQuanChan）的运维脚本

#### 软件架构
1.这个仓库不是一个脚本，而是多个运维小脚本。  
2.这些运维小脚本共用“shared”目录中的共享变量或共享函数。  
3.有些脚本比较复杂，由“一个目录 + 一个*.sh文件”组成，目录中往往包含“usage.sh”的帮助说明函数，比如：  
    (1)“firewall目录 + firewall.sh”，打开firewall目录，从usage.sh文件可见：脚本用于 {设置|显示} Linux操作系统的 netfilter 防火墙规则；  
    (2)“ssh_trust目录 + ssh_trust.sh”，打开ssh_trush目录，从usage.sh文件可见：脚本用于配置操作系统之间的SSH免密码登录；  
    (3)...等等  
4.有些脚本比较简单，只由一个*.sh文件组成，比如：  
    (1)CentOS7_initialization.sh，这个脚本用于初始化CentOS7操作系统；  
    (2)iftop.sh，这个脚本用Linux中的iftop命令查询网卡的流量速度；  
    (3)...等等  
  
  
#### 安装教程

1.  在Bash shell中执行脚本即可，不需要安装。  
  

#### 使用说明

1.  用bash执行根目录下的*.sh文件即可，比如：  
    bash firewall.sh  
    bash ssh_trush.sh  
    bash CentOS7_initialization.sh  
    bash iftop.sh ens33  
  

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
