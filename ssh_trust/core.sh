fnHostCheck(){
    local c=0 x=0 s h p u i;
    for i in `egrep -av $'^[ \t]{0,}#|^[ \t]{0,}$' "$1"`
    do
        s="`echo ${i} | awk -F, '{print $4,$3}'`"   # 写入/etc/hosts的ip和主机名
        h="`echo ${i} | awk -F, '{print $3}'`"      # hostname
        p="`echo ${i} | awk -F, '{print $2}'`"      # port
        u="`echo ${i} | awk -F, '{print $1}'`"      # user
        if [[ `egrep -ac " ${h} | ${h}$" /etc/hosts` -ne 0 ]]; then
            PWarn "/etc/hosts 文件中已经存在名为 ${h} 的主机记录"
            a='n'
            read -p'是否继续? (y/n 默认:n) ' a       # 若继续，则可能因记录冲突导致程序执行失败
            case $a in
                y|Y|yes|YES)
                    ;;
                *)
                    exit 1;
                    ;;
            esac
        fi

        if [[ "${u}" == "root" ]]; then
            if [[ `egrep -ac "^${h} " /root/.ssh/known_hosts` -ne 0 ]]; then
                PWarn "/root/.ssh/known_hosts 文件中已经存在名为 ${h} 的主机记录"
                a='n'
                read -p'是否继续? (y/n 默认:n) ' a       # 若继续，则可能因记录冲突导致程序执行失败
                case $a in
                    y|Y|yes|YES)
                        ;;
                    *)
                        exit 1;
                        ;;
                esac
            fi
        else
            if [[ `egrep -ac "^${h} " /home/${u}/.ssh/known_hosts` -ne 0 ]]; then
                PWarn "/home/${u}/.ssh/known_hosts 文件中已经存在名为 ${h} 的主机记录"
                a='n'
                read -p'是否继续? (y/n 默认:n) ' a       # 若继续，则可能因记录冲突导致程序执行失败
                case $a in
                    y|Y|yes|YES)
                        ;;
                    *)
                        exit 1;
                        ;;
                esac
            fi
        fi
        
        if (( `egrep -ac "^# ssh -p $p ${u}@${h} 'hostname;pwd;date'" /etc/hosts` == 0 )); then
            echo "# ssh -p $p ${u}@${h} 'hostname;pwd;date'" >> /etc/hosts || exit 1 && ((c++))
        fi

        if (( `egrep -ac "^${s}$" /etc/hosts` == 0 )); then
            echo "${s}" >> /etc/hosts || exit 1 && ((x++))
        fi
    done

    POk "加了 $c 行注释行到 /etc/hosts"
    POk "加了 $x 台主机到 /etc/hosts"
    echo "******************************"
}

fnCopyKey(){
    local c=0 h p u pw i;             # Key成功添加到 c 台主机上
    for i in `egrep -av '^ {0,}#|^ {0,}$' "$1"`
    do
        h="`echo ${i} | awk -F',' '{print $3}'`"
        p="`echo ${i} | awk -F',' '{print $2}'`"
        u="`echo ${i} | awk -F',' '{print $1}'`"
        pw="`echo ${i} | awk -F',' '{print $5}'`"
        expect "${pro_base}/${pro_name}/04ssh-copy.exp" "$p" "$h" "$u" "$pw" || exit 1 && ((c++))
        sleep 1
    done
    POk "SSH密钥已加到 $c 台远程主机上"
}

fnInstallDependence(){
    Eval_cont "type expect" || Eval_exit "yum install -y expect"
    POk "依赖包已安装"
}

fnSshHosts(){
    local h p u i;
    for i in `egrep -av $'^[ \t]{0,}#|^[ \t]{0,}$' "$1"`
    do
        h="`echo ${i} | awk -F, '{print $3}'`"
        p="`echo ${i} | awk -F, '{print $2}'`"
        u="`echo ${i} | awk -F, '{print $1}'`"
        Eval_cont "ssh -p $p ${u}@${h} 'hostname;pwd;date'"
        sleep 0.1;
    done
}
