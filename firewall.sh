#!/usr/bin/bash
pro_base="$(cd `dirname $(readlink -f $0)`;pwd)"        # program base directory. readlink是为了获取脚本的真实路径
pro_root="${pro_base##*/}"                              # 我的程序的根目录名字.   readlink使得ln创建的软链也能正确地执行
pro_name="$(readlink -f $0 | sed 's#^.*/##; s#.sh$##')" # 删除'/'前的路径,删除'.sh'

source ${pro_base}/shared/include.sh || exit 1
Include ${pro_base}/${pro_name}/{usage.sh,core.sh}
Include ${pro_base}/shared/prompt.sh

Help "$@"
[[ $# -ne 1 ]] && Usage && exit 1

case $1 in
    set|se|s)
        Eval_cont Clean_rules
        Eval_cont Set_rules
        Eval_cont Display_rules ;;
    display|displa|displ|disp|dis|di|d)
        Eval_cont Display_rules ;;
    clean|clea|cle|cl|c)
        Eval_cont Clean_rules
        Eval_cont Display_rules ;;
    *)
    Usage && exit 1 ;;
esac
