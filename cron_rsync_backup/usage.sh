Usage()
{
    echo -e "
    /*
     * 说明: cron_rsync_backup 是指 设置crontab定期用rsync进行backup
     * 1.按需修改 [配置文件]:          ${pro_base}/${pro_name}/${pro_name}.conf   # The source and destination cannot both be remote.
     * 2.按需修改 ['主机清单'文件]:    ${target_host_list}                        # 源主机和目标主机不能同时都是远端主机
     * 3.按需修改 ['源文件清单']:      ${source_file}                            # 如果要备份本地主机，则使用本地主机的cron_rsync_backup.source文件
     * 4.按需修改 ['排除pattern'文件]: ${exclude_pattern_file}                   # 如果要备份远程主机，则使用远程主机的cron_rsync_backup.source文件
     * Usage: 
     * 执行: \033[1mbash $0 {-f|-b|-s|-h|--help}\033[0m
     *                      -f: 前台执行时使用该option(比如,测试配置是否正确,程序是否能够正常运作)
     *                      -b: 后台执行时使用该option(比如,在crond中执行)
     *                      -s: 设置在crontab每天执行
     */
    "
}                   # $0 总是shell脚本的'路径名/文件名'

Help()              # 若存在 -h 或 --help 选项，则输出 Usage
{
    local i;
    for i
    do
        case $i in
            -h|--help)
                Usage; exit 0;;
            *)
                : ;;
        esac
    done
}