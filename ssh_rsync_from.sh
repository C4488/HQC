Usage()
{
    echo -e "
    /*
     * 说明：
     * 1.${pro_name} 是指 从已经部署了 SSH 免密登陆的远程主机通过 rsync 复制一个或多个'目录|文件'到``本地``
     * 2.通常需要修改 [排除pattern文件]: ${pro_base}/ssh_rsync/ssh_rsync.exclude
     * Usage: 
     * 1.执行: \033[1mbash $0 'target目录,target主机,target端口1' 'target目录,target主机,target端口2' ...\033[0m
     */
    "
}                   # $0 总是shell脚本的'路径名/文件名'

Help()              # 若存在 -h 或 --help 选项，则输出 Usage
{
    local i;
    for i
    do
        case $i in
            -h|--help)
                Usage; exit 0;;
            *)
                : ;;
        esac
    done
}

pro_base="$(cd `dirname $(readlink -f $0)`;pwd)"        # program base directory. readlink是为了获取脚本的真实路径
#pro_root="${pro_base##*/}"                              # 我的程序的根目录名字.   readlink使得ln创建的软链也能正确地执行
pro_name="$(readlink -f $0 | sed 's#^.*/##; s#.sh$##')" # 删除'/'前的路径,删除'.sh'

source ${pro_base}/shared/include.sh || exit 1
Include ${pro_base}/shared/prompt.sh

Help "$@"
[[ $# -lt 1 ]] && Usage && exit 1

for i
do
    [[ $(echo $i | awk -F, '{print NF}') != 3 ]] && Usage && exit 1
    ! [[ "${i:0:1}" == "/" ]] && PError "[仅支持绝对路径] 不符要求的参数: $i" && exit 1
done

exclude_file=${pro_base}/ssh_rsync/ssh_rsync.exclude
! [[ -f "${exclude_file}" ]] && PError "[缺失'排除pattern'文件]: ${exclude_file}" && exit 1

{
c=1
for i
do
    host="$(echo $i |awk -F, '{print $2}')"
    port="$(echo $i |awk -F, '{print $3}')"
    dir_file="$(echo $i |awk -F, '{print $1}')"
    PInfo "同步之前先以演示模式跑一遍第${c}个目录或文件: ${i}"
    ! Eval_cont "rsync -aRnv --del --exclude-from=${exclude_file} -e'ssh -p${port}' ${host}:${dir_file} /" && PError "可能会同步失败,退出程序" && exit 1
    read -p "$(PNote "是否要执行同步操作,请确认? (y/n): ")" ans
    if [[ ${ans} == y || ${ans} == Y ]]; then
        rsync -aR --del --exclude-from=${exclude_file} -e"ssh -p${port}" ${host}:${dir_file} / &&
        POk "成功把 ${host}:${dir_file} 同步到本地" ||
        PError "没能把 ${host}:${dir_file} 同步到本地"
    fi
    ((c++))
    echo
done
}
