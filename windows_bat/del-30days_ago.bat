:: 该脚本保存于 D:\Park_Server
:: 脚本效果：删除 D:\Park_Server\Server\ParkGateServer 目录和子目录中30天前的文件
:: 使用方法:双击bat文件运行即可.每次双击执行前,先备份此脚本，防止30天后把“自己”删除了
forfiles /P "D:\Park_Server\Server\ParkGateServer" /S /D -30 /C "cmd /c echo deleting @path ... && del /q /f @path" > ./ParkGateServer.del.log
forfiles /P "D:\Park_Server\Server\ParkMainServer" /S /D -30 /C "cmd /c echo deleting @path ... && del /q /f @path" > ./ParkMainServer.del.log
:: /P 指定目录
:: /S 递归删除子目录
:: /D 指定日期
:: /C 执行指定命令
:: del /q 不弹出确认提示 /f 强制删除只读文件