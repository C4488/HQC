[[ $# != 1 ]] && echo "Usage: bash $0 interface_name" && exit 1
iftop -nNPB -o 2s -i $1
